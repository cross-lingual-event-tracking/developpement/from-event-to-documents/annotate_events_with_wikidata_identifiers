# Evaluate Corpus annotations

This is a Flask application used to evaluate annotation of events in a corpus of documents. The corpus follows the specification of the [`document_tracking_resources`](https://gitlab.univ-lr.fr/cross-lingual-event-tracking/developpement/from-documents-to-events/documents_tracking_resources) package.

After a manual annotation of events (this means after having manually associated Wikidata IDs to Corpus Event Ids (to know which real world event is mentioned in the documents)), these annotations have to be evaluated. This is the purpose of this software. Using a unique UI, a set of evaluators will be evaluating each event individually and will have to say whether the annotated event is the correct one or not.

It takes as input a CSV file that is extracted from this corpus and has the following content. The `wikidata_event_id`, `real_event`, `specific_event_on_wikipedia` and `comment` are added by the annotator. The `cluster_id` comes from the Corpus used, as well as the `event_id`. The `min_date` and `max_date` are the first and last dates of articles that mention the event. The headlines group together all the headlines of the event, each in his language.

```csv
,cluster_id,event_id,min_date,max_date,headlines_eng,headlines_spa,headlines_deu,wikidata_event_id
2,1256,1493387,2014-10-11 20:48:00,2014-10-27 01:09:00,,"['Euro 2016: Holanda encaja su segunda derrota e Italia no brilla pero vence', 'Bélgica empata a uno con Bosnia y Herzegovina', ...]",https://www.wikidata.org/wiki/Q12342217,True,True,
```

This file is placed in `data`.

# Labels evaluation

Labeling app can be run from a container, use the following commands to start one on port 8080.

```bash
podman build -t corpus_annotation_validation_tool .
```

```bash
podman run -p 8080:8080 -v "./data:/app/data" --rm --name texts_to_events corpus_annotation_validation_tool:latest
```