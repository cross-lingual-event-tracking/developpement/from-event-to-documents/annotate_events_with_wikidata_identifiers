#!/bin/bash

declare -r CONTAINER_NAME="texts_to_events_annotation_service"
declare -r POD_NAME="texts_to_events_pod"

podman stop -t 10 "${CONTAINER_NAME}"
podman rm "${CONTAINER_NAME}"
podman pod rm "${POD_NAME}"
podman pod create --name="${POD_NAME}" --share net -p 8080:8080
podman run --name="${CONTAINER_NAME}" \
           -d \
           --pod="${POD_NAME}" \
           --label io.podman.compose.project="${CONTAINER_NAME}" \
           -v texts_to_events_annotations:/app/labels \
           -m 2048M \
           --restart always \
           localhost/texts_to_events:latest