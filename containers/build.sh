#!/bin/bash

SCRIPT_DIRECTORY="$(realpath "$(dirname "${BASH_SOURCE[0]}")")"

buildah bud  -t texts_to_events:latest --ignorefile "${SCRIPT_DIRECTORY}"/.containerignore -f "${SCRIPT_DIRECTORY}/Dockerfile" "${SCRIPT_DIRECTORY}/.."