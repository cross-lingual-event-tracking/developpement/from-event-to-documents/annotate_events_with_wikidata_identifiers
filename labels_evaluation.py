#!/usr/bin/env python3
import html
import os
import random
import re
from ast import literal_eval
from pathlib import Path
from typing import List, Optional
from urllib.parse import quote

import flask
import numpy as np
import requests
from flask import Flask, render_template, request, url_for, redirect
from pandas import DataFrame, read_csv, Series
import secrets
from flask_login import LoginManager, UserMixin, login_user, current_user, logout_user, login_required

app = Flask(__name__, template_folder="templates")
app.secret_key = secrets.token_hex()

login_manager = LoginManager()
login_manager.init_app(app)
login_manager.login_view = "index"

dataset: DataFrame = None


class User(UserMixin):

    ANNOTATION_DIRECTORY = "labels"

    def __init__(self, username):
        self.id = html.escape(str(username).lower())

        # Load user annotations
        self._annotation_file_path = Path(os.getcwd()) / User.ANNOTATION_DIRECTORY / f"{self.get_id()}.csv"
        if Path(self._annotation_file_path).is_file():
            self.annotations = read_csv(self._annotation_file_path, index_col=0)
        else:
            self.annotations = DataFrame(
                index=dataset.index,
                columns=["correct_wikidata_id", "real_event_agree", "specific_event_agree"]
            )
            self._annotation_file_path.parent.mkdir(parents=True, exist_ok=True)
            self.annotations.to_csv(self._annotation_file_path)

    def save_annotations(self):
        self.annotations.to_csv(self._annotation_file_path)

    def not_labeled_events(self):
        return self.annotations[self.annotations.isna().any(axis=1)].index

    def __str__(self):
        return self.get_id()


@login_manager.user_loader
def load_user(username):
    return User(username)


@app.route('/login', methods=['POST'])
def login():
    username = request.form.get('username', None)
    if username is not None:
        login_user(load_user(username))
        return flask.redirect(flask.url_for('randomly_select_event'))
    return flask.redirect(flask.url_for('index'))


@app.route("/logout", methods=["GET"])
def logout():
    logout_user()
    return flask.redirect(flask.url_for("index"))


def int_converter(x):
    return True if x == "True" else False


def list_converter(x):
    return literal_eval(x) if x else None


def load_dataset(dataset_path: str) -> DataFrame:
    dataset_with_wikidata: DataFrame = read_csv(
        dataset_path,
        index_col=0,
        converters={
            "real_event": int_converter,
            "specific_event_on_wikipedia": int_converter,
            "headlines_spa": list_converter,
            "headlines_eng": list_converter,
            "headlines_deu": list_converter,
        },
    )
    return dataset_with_wikidata


def get_wikidata_id_from_wikidata_url(wikidata_url: str) -> str:
    wikidata_entity_ids = re.findall(r"Q[0-9]+", wikidata_url)
    if len(wikidata_entity_ids) == 1:
        return wikidata_entity_ids[0]
    raise RuntimeError(f"Wikidata URL is not correct: {wikidata_url}")


def build_wikipedia_url_from_page_name_in_language(wikipedia_article_name: str, language: str) -> str:
    return f"https://{language}.wikipedia.org/wiki/{quote(wikipedia_article_name)}"


def get_wikipedia_url_from_wikidata_id(
    wikidata_url: str, language_preferences_order: List[str] = None
) -> Optional[str]:
    if language_preferences_order is None:
        language_preferences_order = ["en", "es", "fr", "de", "it"]
    wikidata_id = get_wikidata_id_from_wikidata_url(wikidata_url)
    url = (
        "https://www.wikidata.org/w/api.php"
        "?action=wbgetentities"
        "&props=sitelinks"
        f"&ids={wikidata_id}"
        "&format=json"
    )
    json_response = requests.get(url).json()
    site_links = json_response.get("entities", dict()).get(wikidata_id, dict()).get("sitelinks", dict())
    wikipedia_pages = {language.removesuffix("wiki"): link.get("title", "") for language, link in site_links.items()}
    for language in language_preferences_order:
        if language in wikipedia_pages:
            return build_wikipedia_url_from_page_name_in_language(wikipedia_pages.get(language), language)


@app.route("/")
def index():
    if current_user.is_active and not current_user.is_anonymous:
        return flask.redirect("random_select_event")
    return render_template(
        "login.html.jinja2",
    )


@app.route("/random_select_event", methods=["GET"])
@login_required
def randomly_select_event():
    not_labeled_events = current_user.not_labeled_events()
    if len(not_labeled_events) > 0:
        return redirect(url_for("display_event_id", event_id=random.choice(not_labeled_events)))
    else:
        return render_template(
            "all_events_annotated.html.jinja2"
        )


@app.route("/<int:event_id>", methods=["GET", "POST"])
@login_required
def display_event_id(event_id: str):
    if request.method == "GET":
        event: Series = dataset.loc[event_id]
        context = {
            "event_id": event_id,
            "wikipedia_url": get_wikipedia_url_from_wikidata_id(event.wikidata_event_id)
            if event.wikidata_event_id is not np.nan
            else "",
            "nb_remaining_events": len(current_user.not_labeled_events()),
        }
        events_infos_as_dict_without_nan = {
            key: value for key, value in event.to_dict().items() if value is not np.nan
        }
        for key, value in events_infos_as_dict_without_nan.copy().items():
            if isinstance(value, list) and len(value) > 20:
                events_infos_as_dict_without_nan[key] = random.sample(value, 20)

        return render_template(
            "event_detail.html.jinja2",
            **{**context, **events_infos_as_dict_without_nan},
        )
    elif request.method == "POST":
        correct_wikidata_id = "incorrect_wikidata_id" not in request.form.keys()
        current_user.annotations.at[event_id, "correct_wikidata_id"] = int(correct_wikidata_id)
        current_user.annotations.at[event_id, "real_event_agree"] = int("real_event" in request.form.keys())
        current_user.annotations.at[event_id, "specific_event_agree"] = int("specific_event_on_wikipedia" in request.form.keys())
        current_user.save_annotations()
        return redirect(url_for("index"))


@app.route("/skip/<int:event_id>", methods=["GET"])
@login_required
def skip_event(event_id):
    current_user.annotations.drop(event_id, inplace=True)
    current_user.save_annotations()
    return flask.redirect(url_for("randomly_select_event"))


if __name__ == "__main__":
    dataset = load_dataset(os.getcwd() + "/data/annotated_events.csv")
    app.run(host="0.0.0.0", port=8080)
